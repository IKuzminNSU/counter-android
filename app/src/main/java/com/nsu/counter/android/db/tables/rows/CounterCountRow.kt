package com.nsu.counter.android.db.tables.rows

import org.jetbrains.anko.db.INTEGER

object CounterCountRow : RowMetadata(
        "counter_count",
        INTEGER
)