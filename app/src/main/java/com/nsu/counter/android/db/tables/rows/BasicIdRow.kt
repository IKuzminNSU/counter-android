package com.nsu.counter.android.db.tables.rows

import org.jetbrains.anko.db.INTEGER
import org.jetbrains.anko.db.PRIMARY_KEY
import org.jetbrains.anko.db.UNIQUE

object BasicIdRow : RowMetadata(
        "id",
        INTEGER + PRIMARY_KEY + UNIQUE
)