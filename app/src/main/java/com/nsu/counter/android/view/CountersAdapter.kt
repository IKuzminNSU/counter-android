package com.nsu.counter.android.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import com.nsu.counter.android.R
import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.presenter.CountersViewPresenter

class CountersAdapter(private val presenter: CountersViewPresenter) : RecyclerView.Adapter<CountersViewHolder>() {
    lateinit var counters: List<Counter>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountersViewHolder {
        val linearLayout: LinearLayout = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_counter, parent, false) as LinearLayout
        return CountersViewHolder(linearLayout)
    }

    override fun getItemCount(): Int {
        return counters.size
    }

    override fun onBindViewHolder(holder: CountersViewHolder, position: Int) {
        val counter = counters[position]
        holder.apply {
            name.text = counter.name
            description.text = counter.descriptionString
            count.text = counter.count.toString()
        }
        holder.itemView.setOnClickListener { presenter.counterItemPressed(position) }
    }
}