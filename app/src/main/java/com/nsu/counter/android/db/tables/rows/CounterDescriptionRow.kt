package com.nsu.counter.android.db.tables.rows

import org.jetbrains.anko.db.TEXT

object CounterDescriptionRow : RowMetadata(
        "counter_description",
        TEXT
)