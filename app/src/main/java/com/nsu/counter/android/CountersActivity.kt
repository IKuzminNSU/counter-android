package com.nsu.counter.android

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.nsu.counter.android.view.CountersAdapter
import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.presenter.CountersViewPresenter
import com.nsu.counter.core.presenter.factory.CountersViewPresenterFactory
import com.nsu.counter.core.view.CountersView

class CountersActivity : BaseActivity<CountersViewPresenter>(), CountersView {
    override val presenter: CountersViewPresenter = CountersViewPresenterFactory(this).getPresenter()
    private lateinit var addCounterButton: FloatingActionButton
    private lateinit var countersListView: RecyclerView
    private val adapter = CountersAdapter(presenter)

    override fun displayCounters(counters: List<Counter>) {
        adapter.counters = counters
        adapter.notifyDataSetChanged()
    }

    override fun openCounterScreen(counter: Counter) {
        val intent = Intent(this, CounterActivity::class.java)
        intent.putExtra(counterIdKey, counter.id)
        startActivity(intent)
    }

    override fun openCreateCounterScreen() {
        startActivity(Intent(this, CreateCounterActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_counters)
        initBinding()
        super.onCreate(savedInstanceState)
        initListeners()
    }

    private fun initBinding() {
        addCounterButton = findViewById(R.id.add_counter_button)
        countersListView = findViewById(R.id.counters_recycler_view)
        countersListView.adapter = adapter
    }

    private fun initListeners() {
        addCounterButton.setOnClickListener { presenter.createCounterButtonPressed() }
        countersListView.layoutManager = LinearLayoutManager(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.countersSetChanged()
    }

    private val Counter.id
        get() = this.toDBModel().id

    companion object {
        const val counterIdKey = "counter_id"
    }
}
