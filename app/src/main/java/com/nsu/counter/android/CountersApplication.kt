package com.nsu.counter.android

import android.app.Application
import com.nsu.counter.android.integration.CountersDbDriverImpl
import com.nsu.counter.android.integration.StringProviderImpl
import com.nsu.counter.core.integration.IntegrationObject

class CountersApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initializeIntegrationObject()
    }

    private fun initializeIntegrationObject() {
        IntegrationObject.stringProvider = StringProviderImpl(this)
        IntegrationObject.counterDBDriver = CountersDbDriverImpl(this)
    }
}