package com.nsu.counter.android

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.nsu.counter.core.model.data.Counter
import com.nsu.counter.core.model.repository.factory.CountersRepositoryFactory
import com.nsu.counter.core.presenter.CounterViewPresenter
import com.nsu.counter.core.presenter.factory.CounterViewPresenterFactory
import com.nsu.counter.core.view.CounterView

class CounterActivity : BaseActivity<CounterViewPresenter>(), CounterView {
    override lateinit var presenter: CounterViewPresenter
    private lateinit var name: EditText
    private lateinit var description: EditText
    private lateinit var count: TextView
    private lateinit var increment: Button
    private lateinit var decrement: Button

    override fun getCount(): Int {
        return count.text.toString().toInt()
    }

    override fun getDescriptionString(): String {
        return description.text.toString()
    }

    override fun getName(): String {
        return name.text.toString()
    }

    override fun setCount(count: Int) {
        this.count.text = count.toString()
    }

    override fun setDescriptionString(descriptionString: String) {
        description.setText(descriptionString)
    }

    override fun setName(name: String) {
        this.name.setText(name)
    }

    override fun showCountersView() {
        finish()
    }

    override fun showInvalidDataWarning(message: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.create_counter_invalid_data_warning_title)
            .setMessage(message)
            .setPositiveButton(R.string.create_counter_unsaved_data_warning_positive_button) { dialog, _ ->
                dialog.dismiss()
                finish()
            }
            .setNegativeButton(R.string.create_counter_unsaved_data_warning_negative_button) { dialog, _ ->
                dialog.dismiss()
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_counter)
        val counter = getCounterFromExtras()
        presenter = CounterViewPresenterFactory(this, counter).getPresenter()
        initBindings()
        super.onCreate(savedInstanceState)
        initListeners()
    }

    private fun getCounterFromExtras(): Counter {
        val id = intent.extras[CountersActivity.counterIdKey] as Int
        return CountersRepositoryFactory().getRepository().findById(id)
    }

    private fun initBindings() {
        name = findViewById(R.id.counter_name_edittext)
        description = findViewById(R.id.counter_description_edittext)
        count = findViewById(R.id.counter_count_textview)
        increment = findViewById(R.id.counter_increment_button)
        decrement = findViewById(R.id.counter_decrement_button)
    }

    private fun initListeners() {
        increment.setOnClickListener { presenter.incrementButtonPressed() }
        decrement.setOnClickListener { presenter.decrementButtonPressed() }
    }

    override fun onBackPressed() {
        presenter.leaveAttemptPerformed()
    }
}