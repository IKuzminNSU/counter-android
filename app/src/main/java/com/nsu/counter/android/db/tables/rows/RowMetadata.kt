package com.nsu.counter.android.db.tables.rows

import org.jetbrains.anko.db.SqlType

open class RowMetadata(val name: String, val params: SqlType)