package com.nsu.counter.android.db.tables

import com.nsu.counter.android.db.tables.rows.*

object CountersTableMetadata : TableMetadata(
        "counters",
        true,
        arrayOf(
                BasicIdRow,
                CounterNameRow,
                CounterDescriptionRow,
                CounterCountRow
        )
) {
    val idRow = BasicIdRow
    val nameRow = CounterNameRow
    val descriptionRow = CounterDescriptionRow
    val countRow = CounterCountRow
}