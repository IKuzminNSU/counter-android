package com.nsu.counter.android

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.nsu.counter.core.presenter.Presenter

abstract class BaseActivity<T : Presenter<*>> : AppCompatActivity() {
    abstract val presenter: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate()
    }
}