package com.nsu.counter.android.integration

import android.content.Context
import com.nsu.counter.android.R
import com.nsu.counter.core.i18n.StringProvider

class StringProviderImpl(private val appContext: Context) : StringProvider {
    override val countIsNegativeWarning: String
        get() = appContext.getString(R.string.count_is_negative_warning)

    override val createCounterUnsavedDataWarning: String
        get() = appContext.getString(R.string.create_counter_unsaved_data_warning_message)

    override val nameIsBlankWarning: String
        get() = appContext.getString(R.string.name_is_blank_warning)
}