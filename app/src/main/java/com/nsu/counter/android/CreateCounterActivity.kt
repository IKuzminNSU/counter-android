package com.nsu.counter.android

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.nsu.counter.core.presenter.CreateCounterPresenter
import com.nsu.counter.core.presenter.factory.CreateCounterPresenterFactory
import com.nsu.counter.core.view.CreateCounterView

class CreateCounterActivity : BaseActivity<CreateCounterPresenter>(), CreateCounterView {
    private lateinit var name: EditText
    private lateinit var description: EditText
    private lateinit var count: EditText
    private lateinit var save: Button

    override val presenter: CreateCounterPresenter = CreateCounterPresenterFactory(this).getPresenter()

    override fun getCount(): Int {
        val str = count.text.toString()
        return if (str.isBlank()) {
            0
        } else {
            str.toInt()
        }
    }

    override fun getDescriptionString(): String {
        return description.text.toString()
    }

    override fun getName(): String {
        return name.text.toString()
    }

    override fun showCountersView() {
        finish()
    }

    override fun showInvalidDataWarning(message: String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.create_counter_invalid_data_warning_title)
            .setMessage(message)
            .setNeutralButton(R.string.create_counter_invalid_data_warning_neutral_button) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    override fun showLeaveWarningDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.create_counter_unsaved_data_warning_title)
            .setMessage(R.string.create_counter_unsaved_data_warning_message)
            .setPositiveButton(R.string.create_counter_unsaved_data_warning_positive_button) { dialog, which ->
                dialog.dismiss()
                presenter.leaveConfirmed()
            }
            .setNegativeButton(R.string.create_counter_unsaved_data_warning_negative_button) { dialog, which ->
                dialog.dismiss()
            }
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_create_counter)
        initBindings()
        super.onCreate(savedInstanceState)
        initListeners()
    }

    private fun initBindings() {
        name = findViewById(R.id.counter_name_edittext)
        description = findViewById(R.id.counter_description_edittext)
        count = findViewById(R.id.counter_count_edittext)
        save = findViewById(R.id.save_counter)
    }

    private fun initListeners() {
        save.setOnClickListener { presenter.tryToSaveCounter() }
    }

    override fun onBackPressed() {
        presenter.leaveAttemptPerformed()
    }
}