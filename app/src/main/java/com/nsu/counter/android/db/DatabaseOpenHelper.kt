package com.nsu.counter.android.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.nsu.counter.android.db.tables.CountersTableMetadata
import com.nsu.counter.android.db.tables.TableMetadata
import org.jetbrains.anko.db.*

class DatabaseOpenHelper(context: Context) : ManagedSQLiteOpenHelper(
        context,
        databaseName,
        null,
        actualDatabaseVersion
) {
    override fun onCreate(db: SQLiteDatabase) {
        createCountersTable(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {  }

    private fun createCountersTable(db: SQLiteDatabase) = createTable(db, CountersTableMetadata)

    private fun createTable(db: SQLiteDatabase, tableMetadata: TableMetadata) {
        db.createTable(
                tableMetadata.name,
                tableMetadata.createIfNotExist,
                *(tableMetadata.rowsMetadata.map { it.name to it.params }.toTypedArray())
        )
    }

    companion object {
        private const val databaseName = "COUNTERS_DATABASE"
        private const val actualDatabaseVersion = 1
        private var instance: DatabaseOpenHelper? = null

        fun getInstance(context: Context): DatabaseOpenHelper {
            if (instance == null) {
                instance = DatabaseOpenHelper(context.applicationContext)
            }
            return instance!!
        }
    }
}

val Context.database: DatabaseOpenHelper
    get() = DatabaseOpenHelper.getInstance(this)