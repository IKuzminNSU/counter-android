package com.nsu.counter.android.db.tables

import com.nsu.counter.android.db.tables.rows.RowMetadata

open class TableMetadata(
        val name: String,
        val createIfNotExist: Boolean = false,
        val rowsMetadata: Array<RowMetadata>
)