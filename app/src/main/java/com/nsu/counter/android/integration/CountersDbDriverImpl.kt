package com.nsu.counter.android.integration

import android.content.Context
import com.nsu.counter.android.db.database
import com.nsu.counter.android.db.tables.CountersTableMetadata
import com.nsu.counter.core.model.CounterDBDriver
import com.nsu.counter.core.model.dbmodel.DBCounter
import org.jetbrains.anko.db.*

class CountersDbDriverImpl(appContext: Context) : CounterDBDriver {
    private val db = appContext.database
    private val metadata = CountersTableMetadata

    override fun delete(model: DBCounter) {
        db.use {
            delete(metadata.name,
                    "${metadata.idRow.name}={modelId}",
                    "modelId" to model.id)
        }
    }

    override fun insert(model: DBCounter) {
        db.use {
            insert(metadata.name,
                    metadata.nameRow.name to model.name,
                    metadata.descriptionRow.name to model.descriptionString,
                    metadata.countRow.name to model.count
            )
        }
    }

    override fun patch(model: DBCounter) {
        db.use {
            update(metadata.name,
                    metadata.countRow.name to model.count,
                    metadata.descriptionRow.name to model.descriptionString,
                    metadata.nameRow.name to model.name)
                    .whereArgs("${metadata.idRow.name}={modelId}",
                            "modelId" to model.id)
                    .exec()
        }
    }

    override fun select(predicate: (DBCounter) -> Boolean): List<DBCounter> {
        return selectAll().filter(predicate)
    }

    override fun selectAll(): List<DBCounter> {
        return db.use {
            val result = select(metadata.name).exec {
                val rowParser = classParser<DBCounter>()
                parseList(rowParser)
            }
            close()
            return@use result
        }
    }
}