package com.nsu.counter.android.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.nsu.counter.android.R
import kotlinx.android.synthetic.main.viewholder_counter.view.*

class CountersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView
    val description: TextView
    val count: TextView
    init {
        name = view.findViewById(R.id.counter_name_textview)
        description = view.findViewById(R.id.counter_description_textview)
        count = view.findViewById(R.id.counter_count_textview)
    }
}