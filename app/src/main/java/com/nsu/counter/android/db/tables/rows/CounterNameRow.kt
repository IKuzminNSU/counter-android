package com.nsu.counter.android.db.tables.rows

import org.jetbrains.anko.db.TEXT

object CounterNameRow : RowMetadata(
        "counter_name",
        TEXT
)